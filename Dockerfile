FROM node as base
WORKDIR /app
COPY . .
# EXPOSE 5000
EXPOSE 8080

FROM base as dev
ENV NODE_ENV=development
RUN npm ci
CMD ["npm", "run", "dev"]

FROM base as production
ENV NODE_ENV=production
ENV npm_config_cache=/tmp
RUN npm ci --production
USER node
CMD ["npm", "start"]