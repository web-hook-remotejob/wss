This service sends update via websocket when it receives event from NATS
Include releaserc.json

docker buildx build --platform linux/arm64/v8,linux/amd64 -t registry.gitlab.com/web-hook-remotejob/wss:latest . --push
docker buildx build --platform linux/arm64/v8,linux/amd64 -t registry.gitlab.com/web-hook-remotejob/wss:0.0.56 . --push
docker buildx build --platform linux/arm64/v8,linux/amd64 -t registry.gitlab.com/web-hook-remotejob/wss:0.0.57 . --push
docker buildx build --platform linux/arm64/v8,linux/amd64 -t registry.gitlab.com/web-hook-remotejob/wss:0.0.58 . --push


