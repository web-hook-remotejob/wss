## [1.0.4](https://gitlab.com/web-hook-remotejob/wss/compare/v1.0.3...v1.0.4) (2023-01-25)


### Bug Fixes

* before main ([3b6ad82](https://gitlab.com/web-hook-remotejob/wss/commit/3b6ad821e22c0fba1c864d4deafaf7bdad30b38f))
* CI ([efcaf0d](https://gitlab.com/web-hook-remotejob/wss/commit/efcaf0d35f8d5d782c501e90373bc759ec1d7e42))
* CI 2 ([d1bd991](https://gitlab.com/web-hook-remotejob/wss/commit/d1bd9915572606656469ccf964e769ff12eae5c4))

## [1.0.3](https://gitlab.com/web-hook-remotejob/wss/compare/v1.0.2...v1.0.3) (2023-01-16)


### Bug Fixes

* Doc ([b698793](https://gitlab.com/web-hook-remotejob/wss/commit/b6987930bb6d50f0fbf93f0cc81ecd739150fd7a))
* new tags ([144a979](https://gitlab.com/web-hook-remotejob/wss/commit/144a979c4966ba4c8bb2fa427a2d5433cd5635c4))

## [1.0.2](https://gitlab.com/web-hook-remotejob/wss/compare/v1.0.1...v1.0.2) (2023-01-16)


### Bug Fixes

* make json ([c8dc142](https://gitlab.com/web-hook-remotejob/wss/commit/c8dc142f8d37298e0f5ae280d104724d3bcd76d7))
* make json 2 ([12cc35d](https://gitlab.com/web-hook-remotejob/wss/commit/12cc35d11eb04cba31d620e2ad23eb8c59fb24bb))
* master main ([82ea8fe](https://gitlab.com/web-hook-remotejob/wss/commit/82ea8feb165d4059ceced33ceb093cd445a6fceb))
