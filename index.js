import { connectToDB } from './lib/db.js'
import { runWebsockertServer } from './lib/wss.js';
import { handleMessages } from './lib/nats.js'

const main = async () => {
  await runWebsockertServer();
  await connectToDB();
  await handleMessages();
};

main();

