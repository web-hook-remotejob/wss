import { createServer } from 'http';
import { parse } from 'url';
import { WebSocketServer } from 'ws';
import { logger } from './log.js'

export async function runWebsockertServer() {

    const server = createServer();
    const wss1 = new WebSocketServer({ noServer: true });
    // const wss2 = new WebSocketServer({ noServer: true });

    wss1.on('connection', function connection(ws) {

        
        logger.info(`new connection`);
        // ...
    });

    // wss2.on('connection', function connection(ws) {
    //     // ...
    // });

    server.on('upgrade', function upgrade(request, socket, head) {
        const { pathname } = parse(request.url);

        if (pathname === '/ws') {
            wss1.handleUpgrade(request, socket, head, function done(ws) {
                wss1.emit('connection', ws, request);
            });
        } else {
            socket.destroy();
        }
    });

    server.listen(8080);
}